package com.xhc.framework.config.shiro;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xhc.common.vo.AjaxResult;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录帐号控制过滤器
 * 可以发现他是调用的isAccessAllowed方法和onAccessDenied方法，只要两者有一个可以就可以了，从名字中我们也可以理解，他的逻辑是这样：先调用isAccessAllowed，如果返回的是true，
 * 则直接放行执行后面的filter和servlet，如果返回的是false，则继续执行后面的onAccessDenied方法，如果后面返回的是true则也可以有权限继续执行后面的filter和servelt。
 * 只有两个函数都返回false才会阻止后面的filter和servlet的执行
 *
 * @author ruoyi
 */
public class KickoutSessionFilter extends AccessControlFilter {
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws IOException {

        HttpServletRequest httpRequest = WebUtils.toHttp(request);
        HttpServletResponse httpResponse = WebUtils.toHttp(response);
         String requestURI = httpRequest.getRequestURI();
         String requestURL = httpRequest.getRequestURL().toString();
        Subject subject = getSubject(request, response);
        System.out.println("sessionid："+subject.getSession().getId());
        System.out.println("requestURI："+requestURI);
        System.out.println("requestURL："+requestURL);

       //已结登录 或者 记住密码
       /* if (!subject.isAuthenticated() || !subject.isRemembered()) {
            // 如果没有登录或用户最大会话数为-1，直接进行之后的流程
            httpResponse.setContentType("application/json");
            httpResponse.setCharacterEncoding("utf-8");
            httpResponse.getWriter().print( new ObjectMapper().writeValueAsString(new AjaxResult().success()));
            return false;
        }*/
        // 当前登录用户
        return true;
    }


}
