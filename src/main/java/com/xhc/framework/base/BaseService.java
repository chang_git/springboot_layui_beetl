package com.xhc.framework.base;

import com.xhc.common.vo.AjaxResult;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public abstract class BaseService {

    /**
     * 返回成功
     */
    public AjaxResult success() {
        return new AjaxResult().success();
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success(String msg) {
        return new AjaxResult().success(msg);
    }
    public AjaxResult success(Object obj) {
        return new AjaxResult().success(obj);
    }

    /**
     * 返回警告的信息
     */
    public AjaxResult warn() {
        return new AjaxResult().warn();
    }

    /**
     * 返回警告的信息
     */
    public AjaxResult warn(String msg) {
        return new AjaxResult().warn(msg);
    }



    /**
     * 是否不为空
     *
     * @author changwei
     * @date 2018年12月19日
     */
    protected boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    /**
     * 是否为空
     *
     * @author changwei
     * @date 2018年12月19日
     */
    protected boolean isEmpty(Object obj) {
        if (obj == null)
            return true;
        if (obj instanceof String) {
            String cs = (String) obj;
            int strLen;
            if (cs == null || (strLen = cs.length()) == 0) {
                return true;
            }
            for (int i = 0; i < strLen; i++) {
                if (Character.isWhitespace(cs.charAt(i)) == false) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map)
            return ((Map<?, ?>) obj).isEmpty();
        if (obj instanceof Collection)
            return ((Collection<?>) obj).isEmpty();
        if (obj instanceof Set)
            return ((Set<?>) obj).isEmpty();
        if (obj instanceof Object[])
            return ((Object[]) obj).length == 0;
        return false;
    }
}
