package com.xhc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@SpringBootApplication()
public class SpringbootBeetlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootBeetlApplication.class, args);
    }

}
