package com.xhc.system.vo;

import java.util.List;

/**
 * layui 插件树 DTree 结构
 * @author changwei
 * @date 2018年12月24日
 */
public class DTree {
	private String id;
	private String parentId;
	private String title;
	private String href;
	private String icon;
	private Integer type;
	private String checkArr="0";
	private Integer operateflag;
	private Integer num;
	private List<DTree> children;

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getParentId() {
		return parentId;
	}


	public void setParentId(String parentId) {
		this.parentId = parentId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getHref() {
		return href;
	}


	public void setHref(String href) {
		this.href = href;
	}


	public String getIcon() {
		return icon;
	}


	public void setIcon(String icon) {
		this.icon = icon;
	}


	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}


	public String getCheckArr() {
		return checkArr;
	}


	public void setCheckArr(String checkArr) {
		this.checkArr = checkArr;
	}


	public Integer getOperateflag() {
		return operateflag;
	}


	public void setOperateflag(Integer operateflag) {
		this.operateflag = operateflag;
	}


	public List<DTree> getChildren() {
		return children;
	}


	public void setChildren(List<DTree> children) {
		this.children = children;
	}


	public DTree(String id, String parentId, String title, String href, String icon, Integer type, String checkArr,
			Integer operateflag, List<DTree> children) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.title = title;
		this.href = href;
		this.icon = icon;
		this.type = type;
		this.checkArr = checkArr;
		this.operateflag = operateflag;
		this.children = children;
	}


	public DTree() {
	}

}
