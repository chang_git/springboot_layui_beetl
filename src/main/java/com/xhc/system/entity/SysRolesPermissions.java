package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_roles_permissions")
public class SysRolesPermissions   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_createuser = "createuser";
	public static final String ALIAS_enterpriseid = "enterpriseid";
	public static final String ALIAS_permission_id = "permission_id";
	public static final String ALIAS_role_id = "role_id";
	public static final String ALIAS_createdate = "createdate";
	public static final String ALIAS_modifydate = "modifydate";
	@AssignID()
	private String id ;
	private String createuser ;
	private String enterpriseid ;
	private String permissionId ;
	private String roleId ;
	private Date createdate ;
	private Date modifydate ;
	
	public SysRolesPermissions() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public String getCreateuser(){
		return  createuser;
	}
	public void setCreateuser(String createuser ){
		this.createuser = createuser;
	}
	
	public String getEnterpriseid(){
		return  enterpriseid;
	}
	public void setEnterpriseid(String enterpriseid ){
		this.enterpriseid = enterpriseid;
	}
	
	public String getPermissionId(){
		return  permissionId;
	}
	public void setPermissionId(String permissionId ){
		this.permissionId = permissionId;
	}
	
	public String getRoleId(){
		return  roleId;
	}
	public void setRoleId(String roleId ){
		this.roleId = roleId;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	
	public Date getModifydate(){
		return  modifydate;
	}
	public void setModifydate(Date modifydate ){
		this.modifydate = modifydate;
	}
	

}
