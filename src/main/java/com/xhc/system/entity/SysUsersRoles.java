package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_users_roles")
public class SysUsersRoles   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_deleteflag = "deleteflag";
	public static final String ALIAS_createuser = "createuser";
	public static final String ALIAS_enterpriseid = "enterpriseid";
	public static final String ALIAS_role_id = "role_id";
	public static final String ALIAS_user_id = "user_id";
	public static final String ALIAS_createdate = "createdate";
	public static final String ALIAS_modifydate = "modifydate";
	@AssignID()
	private String id ;
	private Integer deleteflag ;
	private String createuser ;
	private String enterpriseid ;
	private String roleId ;
	private String userId ;
	private Date createdate ;
	private Date modifydate ;
	
	public SysUsersRoles() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public Integer getDeleteflag(){
		return  deleteflag;
	}
	public void setDeleteflag(Integer deleteflag ){
		this.deleteflag = deleteflag;
	}
	
	public String getCreateuser(){
		return  createuser;
	}
	public void setCreateuser(String createuser ){
		this.createuser = createuser;
	}
	
	public String getEnterpriseid(){
		return  enterpriseid;
	}
	public void setEnterpriseid(String enterpriseid ){
		this.enterpriseid = enterpriseid;
	}
	
	public String getRoleId(){
		return  roleId;
	}
	public void setRoleId(String roleId ){
		this.roleId = roleId;
	}
	
	public String getUserId(){
		return  userId;
	}
	public void setUserId(String userId ){
		this.userId = userId;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	
	public Date getModifydate(){
		return  modifydate;
	}
	public void setModifydate(Date modifydate ){
		this.modifydate = modifydate;
	}
	

}
