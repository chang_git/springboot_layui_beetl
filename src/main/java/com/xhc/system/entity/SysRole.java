package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_role")
public class SysRole   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_deleteflag = "deleteflag";
	public static final String ALIAS_isoriginaldata = "isoriginaldata";
	public static final String ALIAS_operateflag = "operateflag";
	public static final String ALIAS_createuser = "createuser";
	public static final String ALIAS_enterpriseid = "enterpriseid";
	public static final String ALIAS_remark = "remark";
	public static final String ALIAS_rolename = "rolename";
	public static final String ALIAS_value = "value";
	public static final String ALIAS_createdate = "createdate";
	public static final String ALIAS_modifydate = "modifydate";
	@AssignID()
	private String id ;
	private Integer deleteflag ;
	/*
	1为原始数据 不可修改和删除
	*/
	private Integer isoriginaldata ;
	/*
	是否启用 -1 不启用
	*/
	private Integer operateflag ;
	private String createuser ;
	private String enterpriseid ;
	/*
	描述
	*/
	private String remark ;
	/*
	角色名称
	*/
	private String rolename ;
	/*
	角色值
	*/
	private String value ;
	private Date createdate ;
	private Date modifydate ;
	
	public SysRole() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public Integer getDeleteflag(){
		return  deleteflag;
	}
	public void setDeleteflag(Integer deleteflag ){
		this.deleteflag = deleteflag;
	}
	
	/**
	* 1为原始数据 不可修改和删除
	*@return 
	*/
	public Integer getIsoriginaldata(){
		return  isoriginaldata;
	}
	/**
	* 1为原始数据 不可修改和删除
	*@param  isoriginaldata
	*/
	public void setIsoriginaldata(Integer isoriginaldata ){
		this.isoriginaldata = isoriginaldata;
	}
	
	/**
	* 是否启用 -1 不启用
	*@return 
	*/
	public Integer getOperateflag(){
		return  operateflag;
	}
	/**
	* 是否启用 -1 不启用
	*@param  operateflag
	*/
	public void setOperateflag(Integer operateflag ){
		this.operateflag = operateflag;
	}
	
	public String getCreateuser(){
		return  createuser;
	}
	public void setCreateuser(String createuser ){
		this.createuser = createuser;
	}
	
	public String getEnterpriseid(){
		return  enterpriseid;
	}
	public void setEnterpriseid(String enterpriseid ){
		this.enterpriseid = enterpriseid;
	}
	
	/**
	* 描述
	*@return 
	*/
	public String getRemark(){
		return  remark;
	}
	/**
	* 描述
	*@param  remark
	*/
	public void setRemark(String remark ){
		this.remark = remark;
	}
	
	/**
	* 角色名称
	*@return 
	*/
	public String getRolename(){
		return  rolename;
	}
	/**
	* 角色名称
	*@param  rolename
	*/
	public void setRolename(String rolename ){
		this.rolename = rolename;
	}
	
	/**
	* 角色值
	*@return 
	*/
	public String getValue(){
		return  value;
	}
	/**
	* 角色值
	*@param  value
	*/
	public void setValue(String value ){
		this.value = value;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	
	public Date getModifydate(){
		return  modifydate;
	}
	public void setModifydate(Date modifydate ){
		this.modifydate = modifydate;
	}
	

}
