package com.xhc.system.dao;

import com.xhc.system.entity.SysMethodExecuteDate;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
@SqlResource("system.sysMethodExecuteDate")
public interface SysMethodExecuteDateDao extends BaseMapper<SysMethodExecuteDate> {
}
