package com.xhc.system.dao;

import com.xhc.system.entity.SysUsers;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;


@Component
@SqlResource("system.sysUsers")
public  interface SysUsersDao extends BaseMapper<SysUsers> {

}
