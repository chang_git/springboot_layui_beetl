package com.xhc.system.dao;

import com.xhc.system.entity.SysDepartment;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
@SqlResource("system.sysDepartment")
public interface SysDepartmentDao extends BaseMapper<SysDepartment> {
}
