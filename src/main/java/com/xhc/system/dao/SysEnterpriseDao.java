package com.xhc.system.dao;

import com.xhc.system.entity.SysEnterprise;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
@SqlResource("system.sysEnterprise")
public interface SysEnterpriseDao extends BaseMapper<SysEnterprise> {
}
