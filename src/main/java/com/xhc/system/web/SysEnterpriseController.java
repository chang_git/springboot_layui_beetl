package com.xhc.system.web;

import com.xhc.common.vo.AjaxResult;
import com.xhc.common.vo.GridDataTable;
import com.xhc.framework.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system/enterprise")
public class SysEnterpriseController extends BaseController {

    @Override
    public AjaxResult get(String id) {
        return null;
    }

    @Override
    public AjaxResult delete(String id) {
        return null;
    }

    @Override
    public GridDataTable searchList() {
        return null;
    }
}
