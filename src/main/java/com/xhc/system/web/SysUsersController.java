package com.xhc.system.web;

import com.xhc.common.vo.AjaxResult;
import com.xhc.common.vo.GridDataTable;
import com.xhc.framework.base.BaseController;
import com.xhc.system.entity.SysUsers;
import com.xhc.system.services.SysUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/system/users")
public class SysUsersController extends BaseController {

    @Autowired
    SysUsersService sysUsersService;


    @PostMapping("/saveOrUpdate")
    public AjaxResult saveOrUpdate(SysUsers  sysUsers) {
        return sysUsersService.saveOrUpdate(sysUsers);
    }

    @GetMapping("/get")
    public AjaxResult get(String id) {
        return sysUsersService.get(id);
    }

    @PostMapping("/delete")
    public AjaxResult delete(String id){
        return sysUsersService.delete(id);
    }


    @GetMapping("/searchList")
    public GridDataTable searchList() {
        GridDataTable pageTableData = getPageTableData("system.sysUsers.searchList",SysUsers.class);
        return pageTableData;
    }
}
