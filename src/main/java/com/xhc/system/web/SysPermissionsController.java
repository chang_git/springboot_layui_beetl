package com.xhc.system.web;

import com.xhc.common.vo.AjaxResult;
import com.xhc.common.vo.GridDataTable;
import com.xhc.framework.base.BaseController;
import com.xhc.system.entity.SysPermissions;
import com.xhc.system.entity.SysUsers;
import com.xhc.system.services.SysPermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/system/permissions")
public class SysPermissionsController extends BaseController {
    @Autowired
    SysPermissionsService sysPermissionsService;

    @PostMapping("/saveOrUpdate")
    public AjaxResult saveOrUpdate(SysPermissions sysPermissions) {
        return sysPermissionsService.saveOrUpdate(sysPermissions);
    }

    @GetMapping("/get")
    public AjaxResult get(String id) {
        return sysPermissionsService.get(id);
    }

    @PostMapping("/delete")
    public AjaxResult delete(String id){
        return sysPermissionsService.delete(id);
    }

    @Override
    public GridDataTable searchList() {
        return null;
    }

    @PostMapping("/getTreeData")
    public AjaxResult getTreeData() {
       return  sysPermissionsService.getTreeData();
    }
}
