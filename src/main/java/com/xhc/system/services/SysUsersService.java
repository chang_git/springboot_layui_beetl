package com.xhc.system.services;

import com.xhc.common.utils.RandomUtil;
import com.xhc.common.vo.AjaxResult;
import com.xhc.framework.base.BaseService;
import com.xhc.system.dao.SysUsersDao;
import com.xhc.system.entity.SysUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SysUsersService extends BaseService {
    @Autowired
    SysUsersDao sysUsersDao;

    public AjaxResult get(String id) {
        SysUsers users = sysUsersDao.unique(id);
        return success(users);
    }

    public AjaxResult delete(String id) {
        SysUsers sysUsers = new SysUsers();
        sysUsers.setId(id);
        sysUsers.setDeleteflag(1);
        int i = this.sysUsersDao.updateTemplateById(sysUsers);
        return success();
    }

    public AjaxResult saveOrUpdate(SysUsers sysUsers) {
        if (isNotEmpty(sysUsers.getId())) {
            sysUsersDao.updateTemplateById(sysUsers);
        } else {
            sysUsers.setId(RandomUtil.getUuidByJdk());
            sysUsers.setCreatedate(new Date());
            sysUsersDao.insertTemplate(sysUsers);

        }
        return success();
    }
}
