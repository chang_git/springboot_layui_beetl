package com.xhc.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class RandomUtil {
    //  随机数生成器
    private static  java.util.Random random = new java.util.Random();
    private static  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    

    public static String getUuidByJdk(){
        return UUID.randomUUID().toString().replace("-", "");
	}
    

    /**
     * 生成系统时间+随机数
     * @return
     *
     *@author rj
     *@date  2018-4-12 下午3:58:42
     */
    public static String getRandomDateTime() {
        StringBuffer ran=new StringBuffer(RandomUtil.format.format(new Date()));
        //ran.append();
        return ran.toString();
     }
    
    
    
    public static void main(String[] args) {
    	System.out.println(random.nextInt());
        
    }
}