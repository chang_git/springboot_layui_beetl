layui.define(['cfg', 'layer', 'element', 'form'], function (exports) {
    var cfg = layui.cfg;
    var layer = layui.layer;
    var element = layui.element;
    var form = layui.form;
    var $ = layui.$;

    var Ad = {
        // 中间弹出
        popLayer: function (url, title, para, heigth, width, maxmin) {
            if (!url) {
                layer.msg("url为空", {icon: 5});
                return false;
            }
            if (para && JSON.stringify(para) != "{}"){
                url = url + "?";
                for (var o in para) {
                    if (para[o] != -1) {
                        url += o + "=" + para[o] + "&";
                    }
                }
                url = url.substring(0, url.length-1);
            }
            //Ajax获取
            $.get(url, {}, function (html) {
                maxmin = maxmin ? maxmin : false;
                heigth = heigth ? heigth : '80%';
                width = width ? width : '80%';
                var popupCenterIndex = layer.open({
                    type: 1,
                    title: title ? title : false,
                    skin: 'layui-layer-crownCenter',
                    area: [heigth, width],
                    maxmin:maxmin,
                    content: html + ""  //注意，如果str是object，那么需要字符拼接。
                });

                return popupCenterIndex;

            });
        },
        fromVal: function (filter, object) {
            var formElem = $('.layui-form[lay-filter="' + filter + '"]');
            formElem.each(function () {
                var itemFrom = $(this);
                layui.each(object, function (key, value) {
                    /* if (typeof (value) === 'object') {
                         fromVal(filter, value);//递归
                     }*/
                    var itemElem = itemFrom.find('[name="' + key + '"]');
                    //如果对应的表单不存在，则不执行
                    if (!itemElem[0]) {
                        return;
                    }
                    var type = itemElem[0].type;
                    //如果为复选框
                    if (type === 'checkbox') {
                        if (typeof (value) !== 'object') {
                            itemElem[0].checked = value;
                        } else {
                            layui.each(value, function (index, item) {
                                itemElem.each(function () {
                                    if (this.value === item.toString()) {
                                        this.checked = true;
                                    }
                                });
                            });
                        }
                    } else if (type === 'radio') { //如果为单选框
                        itemElem.each(function () {
                            if (this.value === value.toString()) {
                                this.checked = true;
                            }
                        });
                    } else { //其它类型的表单
                        itemElem.val(value);
                    }
                });
            });
            form.render(null, filter);
        },
        Msg: function (data) {
            if (data) {
                if (data.code == 0) {
                    layer.msg(data.msg, {icon: 6});
                } else if (data.code == 3) {
                    layer.msg(data.msg, {icon: 5});
                    window.location = cfg.loginUrl;
                } else if (data.code == 1) {
                    layer.msg(data.msg, {icon: 5});
                }
            }
        },
        tabDeleteSelf: function () { //关闭本身的页面
            var tabId = self.frameElement.getAttribute('tab-id');
            if (tabId) {
                parent.main.tab.tabDelete(tabId);
            }
        },
        tabDelete: function (tabId) {
            parent.main.tab.tabDelete(tabId);
        },
        tabAdd: function (title, url, id) {
            parent.main.tab.tabAdd(title, url, id);
        },
        get: function (url, params, success) {
            var method = "GET";
            return this.request(url, params, method, success);
        },
        post: function (url, params, success) {
            var method = "POST";
            return this.request(url, params, method, success);
        },
        put: function (url, params, success) {
            var method = "PUT";
            return this.request(url, params, method, success);
        },
        delete: function (url, params, success) {
            var method = "DELETE";
            return this.request(url, params, method, success);
        },
        request: function (url, params, method, success, contentType) {

         /*   if(!cfg.getToken()){
                window.location.href=cfg.getLoginUrll();
                cfg.removeAll();
            }*/

            method = method ? method : "GET";
            if (!contentType) {
                switch (method) {
                    case "GET":
                        contentType = '';
                        break;
                    case "POST":
                        contentType = 'application/x-www-form-urlencoded;charset=UTF-8';
                        break;
                    case "PUT":
                        contentType = 'application/x-www-form-urlencoded;charset=UTF-8';
                        break;
                    case "DELETE":
                        contentType = '';
                        break;
                    default:
                        contentType = '';
                }
            }
            $.ajax({
                url: cfg.serverUrl + url,
                data: params ? params : {},
                type: method,
                contentType: contentType,
                xhrFields: {
                    withCredentials: false
                },
                async: true,
                crossDomain: true,
                success: function (data) {
                    //0 正常  1 错误提示 -1 token 失效
                    // alert(data);
                   /* if(data.code==3){ //没有权限
                        window.location.href='/login.html';
                        cfg.removeAll();
                    }*/

                    success(data);
                },
                error: function (xhr) {
                    // console.log(xhr);
                    //   if (xhr.responseJSON.error === 'UNAUTHORIZED') {
                    //layer.msg(JSON.parse(xhr.responseText).msg, {icon: 5});
                    //  }
                    return false;
                },
                beforeSend: function (xhr) {
                    var token = cfg.getToken();
                    if (token) {
                        xhr.setRequestHeader('token', token);
                    }
                }
            });
        },
        hasPerm: function (buttonAlias) {
            var permButtons = cfg.getPermButtons();
            if (permButtons) {
                for (var i = 0; i < permButtons.length; i++) {
                    if (buttonAlias == permButtons[i]) {
                        return true;
                    }
                }
            }
            return false;
        },
        // 缓存临时数据
        putTempData: function (key, value) {
            if (value) {
                layui.sessionData('tempData', {key: key, value: value});
            }
        },
        // 获取缓存临时数据 获取完删除
        getTempData: function (key) {
            var tempData = layui.sessionData('tempData')[key];
            layui.sessionData('tempData', {key: key, remove: true});
            return tempData;
        },
        getSearchForm: function (searchForm) {
            var val = {};
            var inputVals = $('#' + searchForm).find(':input').filter(function () {
                return $.trim(this.value).length > 0;
            }).serializeArray();
            $(inputVals).each(function () {
                if (val[this.name] !== undefined) {
                    if (!Array.isArray(val[this.name])) {
                        val[this.name] = [val[this.name]];
                    }
                    val[this.name].push(this.value);
                } else {
                    val[this.name] = this.value;
                }
            });
            return val;
        }
    };

    exports('Ad', Ad);
});
