
layui.config({
    base: '/static/lib/layui/module/'
}).extend({ //设定模块别名
    Ad: 'Ad'
    ,cfg: 'cfg'
});

var tableIns;

layui.use(['form','layer','table','cfg','Ad'],function(){
    var $ = layui.jquery;
    var table = layui.table;
    var layer = layui.layer;
    var form = layui.form;
    var  cfg = layui.cfg;
    var  Ad = layui.Ad;

    //行工具栏
    $('#tool').vm({
        editShow: true,
        delShow:true
    });

    //头部工具栏
    $('#toolbar').vm({
        addShow:true
    });



    var cols=[ [ {
    type : "checkbox",
    fixed : "left",
    width : 50
}, {
    field : 'rolename',
    title : '角色名',
    minWidth : 50,
    align : "center"
}, {
    field : 'value',
    title : '角色值',
    minWidth : 50,
    align : "center"
},{
    field : 'remark',
    title : '角色描述',
    minWidth : 80,
    align : "center"
}, {
    field : 'operateflag',
    title : '是否启用',
    align : 'center',
    templet : function(d) {
    if (d.operateflag == 0) {
   		 return "启用";
    } else {
        return "停用";
    }
    }
    }, {
        field : 'createdate',
        title : '创建时间',
        align : 'center',
        minWidth : 150
    }, {
        title : '操作',
        minWidth : 175,
        templet : '#tool',
        fixed : "right",
        align : "center"
    } ] ];

	//用户列表
	 tableIns = table.render({
        elem: '#tableList',
		url :cfg.getTokenUrl( '/system/role/searchList'),
		cellMinWidth : 80,
		page : true,
		height : "full-120",
		limits : [ 10, 15, 20, 25 ],
		limit : 20,
		id : "tableList",
        toolbar: '#toolbar',
		cols :cols

	});

    //监听表单搜索
    form.on('submit(searchBtn)', function(data){
        var formdata=data.field;
        table.reload("tableList",{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where: formdata
        });
        return false;
    });

    //监听头部工具栏
    table.on('toolbar(tableList)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'add':
                $("#selectId").val("");
                Ad.popLayer('/templates/system/role/roleInput.html','添加角色');
                break;
        }
    });

    //监听行工具条
    table.on('tool(tableList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;

        if(layEvent === 'edit'){ //编辑
            $("#selectId").val(data.id);
            Ad.popLayer('/templates/system/role/roleInput.html','编辑角色');
        }else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此角色？',{icon:3, title:'提示信息'},function(index){
                Ad.post("/system/role/delete",{
                    id : data.id  //将需要删除的newsId作为参数传入
                },function(data){
                    Ad.Msg(data);
                    tableIns.reload();
                })
            });
        }
    });

});
