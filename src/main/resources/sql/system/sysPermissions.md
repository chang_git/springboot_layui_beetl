getPermissionsList
===
* 注释

	select #use("cols")# from sys_permissions  where     #use("condition")#
	and  deleteflag=0

cols
===
	title,icon,id,href,spread,target,pid,createdate,deleteflag,type,operateflag,modifydate,num

updateSample
===
	
	title=#title#,icon=#icon#,id=#id#,href=#href#,spread=#spread#,target=#target#,pid=#pid#,createdate=#createdate#,deleteflag=#deleteflag#,type=#type#,operateflag=#operateflag#,modifydate=#modifydate#,num=#num#

condition
===

	1 = 1  
	@if(!isEmpty(title)){
	 and title=#title#
	@}
	@if(!isEmpty(icon)){
	 and icon=#icon#
	@}
	@if(!isEmpty(id)){
	 and id=#id#
	@}
	@if(!isEmpty(href)){
	 and href=#href#
	@}
	@if(!isEmpty(spread)){
	 and spread=#spread#
	@}
	@if(!isEmpty(target)){
	 and target=#target#
	@}
	@if(!isEmpty(pid)){
	 and pid=#pid#
	@}
	@if(!isEmpty(createdate)){
	 and createdate=#createdate#
	@}
	@if(!isEmpty(deleteflag)){
	 and deleteflag=#deleteflag#
	@}
	@if(!isEmpty(type)){
	 and type=#type#
	@}
	@if(!isEmpty(operateflag)){
	 and operateflag=#operateflag#
	@}
	@if(!isEmpty(modifydate)){
	 and modifydate=#modifydate#
	@}
	@if(!isEmpty(num)){
	 and num=#num#
	@}
	
	